package com.glory.weather;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;

public class SearchActivity extends AppCompatActivity implements View.OnClickListener{
    EditText txtSearch;
    Button btnSearch;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_search);
        bindView();
    }

    @Override
    protected void onResume() {
        super.onResume();
        txtSearch.setText("");
    }

    public void bindView() {
        txtSearch = (EditText) findViewById(R.id.txtSearch);
        btnSearch = (Button) findViewById(R.id.btnSearch);
        btnSearch.setOnClickListener(this);
    }
    @Override
    public void onClick(View v) {
        if (v.getId() == R.id.btnSearch) {

            if (txtSearch.getText().toString().trim().matches("")) {

            }
            else {
                Intent intent = new Intent(this, WeatherActivity.class);
                intent.putExtra("city", txtSearch.getText().toString());
                startActivity(intent);
            }
        }
    }
}
