package com.glory.weather;

import android.app.ProgressDialog;
import android.content.Context;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.GridView;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.google.gson.Gson;
import com.loopj.android.http.AsyncHttpClient;
import com.loopj.android.http.TextHttpResponseHandler;

import cz.msebera.android.httpclient.Header;

import com.glory.weather.models.YahooWeather;
import com.glory.weather.models.Forecast;
import java.util.List;

public class WeatherActivity extends AppCompatActivity {

    TextView cityName;
    TextView date;
    ImageView img;
    TextView temp;
    TextView status;
    TextView error;
    GridView gvDailyList;
    ProgressDialog progressDialog;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_weather);

        progressDialog = new ProgressDialog(this);
        progressDialog.setTitle("loading");
        progressDialog.setMessage("please waite...");


        bindView();
    }

    private void bindView() {

        cityName = (TextView) findViewById(R.id.cityName);
        img = (ImageView) findViewById(R.id.img);
        temp = (TextView) findViewById(R.id.temp);
        status = (TextView) findViewById(R.id.status);
        date = (TextView) findViewById(R.id.date);
        gvDailyList = (GridView) findViewById(R.id.gvDailyList);
        error = (TextView) findViewById(R.id.error);

        String city = getIntent().getStringExtra("city");
        cityName.setText(city);

        getWeatherFromYahoo(city);
    }

    private void getWeatherFromYahoo(String city) {
        String url = "https://query.yahooapis.com/v1/public/yql?q=select%20*%20from%20weather.forecast%20where%20woeid%20in%20(select%20woeid%20from%20geo.places(1)%20where%20text%3D%22" + city + "%2C%20ir%22)&format=json&env=store%3A%2F%2Fdatatables.org%2Falltableswithkeys";

        AsyncHttpClient client = new AsyncHttpClient();


        client.get(url, new TextHttpResponseHandler() {
            @Override
            public void onStart() {
                super.onStart();
                progressDialog.show();
            }

            @Override
            public void onFailure(int statusCode, Header[] headers, String responseString, Throwable throwable) {
                error.setText(throwable.toString());
            }

            @Override
            public void onSuccess(int statusCode, Header[] headers, String responseString) {

                parseServerByGSON(responseString);
            }

            @Override
            public void onFinish() {
                super.onFinish();
                progressDialog.dismiss();
            }
        });

    }

    private void parseServerByGSON(String responseString) {

        Gson gson = new Gson();
        YahooWeather yahoo = gson.fromJson(responseString, YahooWeather.class);

        String tempYahoo = yahoo.getQuery().getResults().getChannel().getItem().getCondition().getTemp();
        tempYahoo = convertToCentigrade(tempYahoo)+ "°C";
        temp.setText(tempYahoo);

        String statusYahoo = yahoo.getQuery().getResults().getChannel().getItem().getCondition().getText();
        status.setText(statusYahoo);

        String dateYahoo = yahoo.getQuery().getResults().getChannel().getLastBuildDate();
        date.setText(dateYahoo);

        List<Forecast> forecastsModels = yahoo.getQuery().getResults().getChannel().getItem().getForecast(); ;
        ForecastAdapter adapter = new ForecastAdapter(this,forecastsModels);
        gvDailyList.setAdapter(adapter);

        switch (statusYahoo){
            case "Sunny":
                img.setImageResource(R.drawable.sunny);
                break;
            case "Cloudy":
                img.setImageResource(R.drawable.cloudy);
                break;
            case "Mostly Sunny":
                img.setImageResource(R.drawable.mostly_sunny);
                break;
            case "Partly Cloudy":
                img.setImageResource(R.drawable.partly_cloudy);
                break;
            case "Mostly Cloudy":
                img.setImageResource(R.drawable.mostly_cloudy);
                break;
            case "Showers":
                img.setImageResource(R.drawable.showers);
                break;
            case "Scattered Showers":
                img.setImageResource(R.drawable.rainy);
                break;
            case "Rain":
                img.setImageResource(R.drawable.rainy);
                break;
        }


    }

    public String convertToCentigrade(String fahrenheit){
        double f = Double.parseDouble(fahrenheit);
        double c = (f - 32)/1.8;
        int roundC =(int) c;
        return roundC + "";
    }

}
