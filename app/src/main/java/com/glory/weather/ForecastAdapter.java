package com.glory.weather;

import android.annotation.SuppressLint;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.List;

import com.glory.weather.models.Forecast;

/**
 * Created by Glory on 4/5/2018.
 */

public class ForecastAdapter extends BaseAdapter {
    Context mContext;
    List<Forecast> lForecasts;

    public ForecastAdapter(Context mContext1, List<Forecast> forecasts) {
        this.mContext = mContext1;
        this.lForecasts = forecasts;
    }

    @Override
    public int getCount() {
        return lForecasts.size();
    }

    @Override
    public Object getItem(int position) {
        return lForecasts.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View view, ViewGroup parent) {

        View row = LayoutInflater.from(mContext).inflate(R.layout.daily_list, parent, false);



            TextView tempH = row.findViewById(R.id.tempH);
            tempH.setText(convertToCentigrade(lForecasts.get(position).getHigh()));

            TextView tempL = row.findViewById(R.id.tempL);
            tempL.setText(convertToCentigrade(lForecasts.get(position).getLow()) );

            TextView day2 = row.findViewById(R.id.day2);
            day2.setText(lForecasts.get(position).getDay());

            ImageView img2 = row.findViewById(R.id.img2);
            String status = lForecasts.get(position).getText();

            switch (status) {
                case "Sunny":
                    img2.setImageResource(R.drawable.sunny);
                    break;
                case "Cloudy":
                    img2.setImageResource(R.drawable.cloudy);
                    break;
                case "Mostly Sunny":
                    img2.setImageResource(R.drawable.mostly_sunny);
                    break;
                case "Mostly Clear":
                    img2.setImageResource(R.drawable.mostly_sunny);
                    break;
                case "Partly Cloudy":
                    img2.setImageResource(R.drawable.partly_cloudy);
                    break;
                case "Mostly Cloudy":
                    img2.setImageResource(R.drawable.mostly_cloudy);
                    break;
                case "Showers":
                    img2.setImageResource(R.drawable.showers);
                    break;
                case "Scattered Showers":
                    img2.setImageResource(R.drawable.rainy);
                    break;
                case "Rain":
                    img2.setImageResource(R.drawable.rainy);
                    break;
            }
                return row;
        }

    public String convertToCentigrade(String fahrenheit){
        double f = Double.parseDouble(fahrenheit);
        double c = (f - 32)/1.8;
        int roundC =(int) c;
        return roundC + "";
    }

    }









